# Designing Data Intensive Applications

__Designing Data-Intensive Applications__

The Big Ideas Behind Reliable, Scalable, and Maintainable Systems

By Martin Kleppmann

Publisher: O'Reilly Media

Release Date: March 2017

Pages: 569
